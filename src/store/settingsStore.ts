import { TimerSettings } from './timerSettings';

export interface SettingsStore {
  settings: TimerSettings;
  setSettings: (newSettings: TimerSettings) => void;
}