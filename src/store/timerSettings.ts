export interface TimerSettings {
  stations: number;
  intervals: number;
  intervalLength: number;
  pauseLength: number;
}