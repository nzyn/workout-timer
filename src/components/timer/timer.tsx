import * as React from "react";
import { observer } from "mobx-react-lite";
import { TimerSettings } from "../../store/timerSettings";
import { Container, Header, Icon, Button } from "semantic-ui-react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

interface TimerProps {
  settings: TimerSettings;
}

function startWorkoutTimer(
  intervals: number,
  intervalLength: number,
  pauseLength: number,
  isPause: boolean,
  setTime: (seconds: number) => void
) {
  const intervalEnd = new Date(
    Date.now() + (isPause ? pauseLength : intervalLength) * 1000
  );

  const interval = setInterval(() => {
    const t = intervalEnd.getTime() - Date.now();
    const seconds = Math.floor(t / 1000);
    setTime(seconds);
    if (seconds <= 0) {
      clearInterval(interval);
      if (intervals > 0) {
        startWorkoutTimer(
          intervals - 1,
          intervalLength,
          pauseLength,
          !isPause,
          setTime
        );
      }
    }
  }, 100);

  return interval;
}

export const Timer = observer((props: TimerProps) => {
  const [time, setTime] = useState(props.settings.intervalLength);

  useEffect(() => {
    const interval = startWorkoutTimer(
      props.settings.intervals,
      props.settings.intervalLength,
      props.settings.pauseLength,
      false,
      setTime
    );

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <Container>
      <Container textAlign="center" fluid>
        <Header as="h1" icon>
          <Icon name="clock outline" />
          <div
            style={{
              fontSize: "128pt",
              marginTop: 64
            }}
          >
            {Math.max(0, time)}
          </div>
        </Header>
        <Container textAlign="center" fluid>
          <Link to={"/"}>
            <Button
              tabIndex={5}
              color="red"
              type="button"
              size="massive"
              style={{
                marginTop: 48
              }}
            >
              Stop
            </Button>
          </Link>
        </Container>
      </Container>
    </Container>
  );
});
