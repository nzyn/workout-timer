import * as React from "react";
import { Form, Container, Button, Header, Icon } from "semantic-ui-react";
import { observer } from "mobx-react-lite";
import { SettingsStore } from "../../store/settingsStore";
import { Link } from "react-router-dom";

interface ConfigProps {
  settingsStore: SettingsStore;
}

export const Config = observer((props: ConfigProps) => {
  return (
    <Container>
      <Container textAlign="center" fluid>
        <Header as="h2" icon>
          <Icon name="settings" />
          Timer Settings
          <Header.Subheader>Set up your workout</Header.Subheader>
        </Header>
      </Container>
      <Form size="huge">
        <Form.Field>
          <label>Stations</label>
          <input
            tabIndex={1}
            type="number"
            min={1}
            value={props.settingsStore.settings.stations}
            onChange={e => {
              props.settingsStore.setSettings({
                ...props.settingsStore.settings,
                stations: Number(e.target.value)
              });
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>Intervals per station</label>
          <input
            tabIndex={2}
            type="number"
            min={1}
            value={props.settingsStore.settings.intervals}
            onChange={e => {
              props.settingsStore.setSettings({
                ...props.settingsStore.settings,
                intervals: Number(e.target.value)
              });
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>Interval length (seconds)</label>
          <input
            tabIndex={3}
            type="number"
            min={1}
            value={props.settingsStore.settings.intervalLength}
            onChange={e => {
              props.settingsStore.setSettings({
                ...props.settingsStore.settings,
                intervalLength: Number(e.target.value)
              });
            }}
          />
        </Form.Field>
        <Form.Field>
          <label>Pause length (seconds)</label>
          <input
            tabIndex={4}
            type="number"
            min={1}
            value={props.settingsStore.settings.pauseLength}
            onChange={e => {
              props.settingsStore.setSettings({
                ...props.settingsStore.settings,
                pauseLength: Number(e.target.value)
              });
            }}
          />
        </Form.Field>
        <Container textAlign="center" fluid>
          <Link to={"/timer"}>
            <Button tabIndex={5} color="green" type="button" size="massive">
              Start
            </Button>
          </Link>
        </Container>
      </Form>
    </Container>
  );
});
