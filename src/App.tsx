import * as React from "react";
import "normalize-css";
import "semantic-ui-css/semantic.min.css";
import { BrowserRouter, Route } from "react-router-dom";
import { useLocalStore } from "mobx-react-lite";
import { Timer } from "./components/timer/timer";
import { SettingsStore } from "./store/settingsStore";
import { Config } from "./components/config/config";
import { TimerSettings } from "./store/timerSettings";

const App = () => {
  const settingsStore = useLocalStore(() => {
    return {
      settings: {
        stations: 1,
        intervals: 6,
        intervalLength: 45,
        pauseLength: 15
      } as TimerSettings,
      setSettings(newSettings: TimerSettings) {
        settingsStore.settings = newSettings;
      }
    } as SettingsStore;
  });

  return (
    <div style={{
      padding: "10px 0"
    }}>
      <BrowserRouter>
        <Route
          exact
          path="/"
          render={() => <Config settingsStore={settingsStore} />}
        />
        <Route
          exact
          path="/timer"
          render={() => <Timer settings={settingsStore.settings} />}
        />
      </BrowserRouter>
    </div>
  );
};

export default App;
